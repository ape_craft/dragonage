//
//  PlayerInfoView.m
//  DragonAge
//
//  Created by user on 12/23/13.
//  Copyright (c) 2013 tz. All rights reserved.
//

#import "PlayerInfoView.h"
#import "InvetoryCollectionViewCell.h"
#import "ItemDescription.h"

@interface PlayerInfoView() {
    IBOutlet UICollectionView* _inventoryCollectionView;
    PlayerNew* _player;
}

@end

@implementation PlayerInfoView

@synthesize delegate = _delegate;

+(id) playerInfoView{
    PlayerInfoView* playerInfo = [[[NSBundle mainBundle] loadNibNamed:@"PlayerInfoView" owner:nil options:nil] lastObject];
    if ([playerInfo isKindOfClass:[PlayerInfoView class]]) {
        return playerInfo;
    } else {
        return nil;
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    return self;
}

- (void) awakeFromNib
{
    //[_inventory registerClass:[CustomCollectionView class] forCellWithReuseIdentifier:@"Cell"];
    [_inventoryCollectionView registerNib:[UINib nibWithNibName:@"InvetoryCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
}

- (void) reloadData
{
    
    [_inventoryCollectionView reloadData];
}

- (void) updateWithPlayer:(PlayerNew*) player
{
    _player = player;
    _movesLabel.text = [NSString stringWithFormat:@"%d", _player.movesLeft];
    [_inventoryCollectionView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[_player allItems] count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    InvetoryCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    ItemDescription* itemDescription = [[_player allItems] objectAtIndex:indexPath.row];
    
    NSLog(@"Uploading image with name: %@", itemDescription.name);
    cell.img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", itemDescription.name]];
    cell.backgroundColor = [UIColor blueColor];
    cell.countItem.text = [NSString stringWithFormat:@"%d", itemDescription.countItem];
    return cell;

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if([self.delegate respondsToSelector:@selector(playerInfoView:didSelectItem:)])
    {
        [self.delegate playerInfoView:self didSelectItem:[[_player allItems] objectAtIndex:indexPath.row]];
    }
}

-(void) dealloc {
    [_player release];
    [_inventoryCollectionView release];
    [_movesLabel release];
    [super dealloc];
}

@end
