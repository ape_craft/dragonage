//
//  PlayerInfoView.h
//  DragonAge
//
//  Created by user on 12/23/13.
//  Copyright (c) 2013 tz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerNew.h"

@class Item;
@class PlayerInfoView;

@protocol PlayerInfoViewDelegate <NSObject>

@optional
- (void) playerInfoView:(PlayerInfoView*) playerView didSelectItem:(Item*) item;

@end


@interface PlayerInfoView : UIView <UICollectionViewDataSource, UICollectionViewDelegate>


@property (strong, nonatomic) IBOutlet UILabel* movesLabel;
@property(nonatomic, assign) id<PlayerInfoViewDelegate> delegate;

+(id) playerInfoView;
- (void) updateWithPlayer:(PlayerNew*) player;
- (void) reloadData;

@end
