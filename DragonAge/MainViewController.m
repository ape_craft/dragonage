//
//  MainViewViewController.m
//  DragonAge
//
//  Created by user on 12/21/13.
//  Copyright (c) 2013 tz. All rights reserved.
//

#import "MainViewController.h"
#import "RoomItemsView.h"
#import "PlayerInfoView.h"
#import "GameEngine.h"
#import "Item.h"

@interface MainViewController ()<RoomItemsViewDelegate, PlayerInfoViewDelegate, GameEngineDelegate> {
    IBOutlet UIView* _roomContainerView;
    IBOutlet UIView* _playerInfoContainerView;
    PlayerInfoView* _playerInfoView;
    RoomItemsView* _roomItemsView;
    GameEngine* _gameEngine;
    
    IBOutlet UIButton* _moveUp;
    IBOutlet UIButton* _moveLeft;
    IBOutlet UIButton* _moveDown;
    IBOutlet UIButton* _moveRight;
    
    IBOutlet UILabel* _roomNumber;
}
- (IBAction) newGame: (id) sender;
- (IBAction) PerformMove: (id) sender;
- (void)loadRoomProperties;

@end

@implementation MainViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    _gameEngine = [[GameEngine alloc] initEngineAndItems];
    _gameEngine.delegate = self;
    
    [_gameEngine startNewGame];
    
    _playerInfoView = [PlayerInfoView playerInfoView];
    _roomItemsView = [RoomItemsView roomItemsView];
    
    _roomItemsView.delegate = self;
    _playerInfoView.delegate = self;
    
    [_roomContainerView addSubview:_roomItemsView];
    [_playerInfoContainerView addSubview:_playerInfoView];
    
    [self loadRoomProperties];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_roomContainerView release];
    [_playerInfoContainerView release];
    [_gameEngine release];
    [_roomNumber release];
    [_roomItemsView release];
    [_playerInfoView release];
    [_moveDown release];
    [_moveLeft release];
    [_moveRight release];
    [_moveUp release];
    [super dealloc];
}

- (IBAction)newGame:(id)sender {
    [_gameEngine startNewGame];
    [_playerInfoView setUserInteractionEnabled:YES];
    [_roomItemsView setUserInteractionEnabled:YES];
    [self loadRoomProperties];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)PerformMove:(id)sender {
    UIButton* temp = (UIButton*)sender;
    [_gameEngine goToNextRoomWithDirection:temp.tag];
    [self loadRoomProperties];
}

-(void)loadRoomProperties {
    
    _moveDown.enabled = false;
    _moveUp.enabled = false;
    _moveRight.enabled = false;
    _moveLeft.enabled = false;
    
    NSDictionary* dictionary = _gameEngine.currentRoom.directions;
    for (id dir in dictionary) {
        Direction direction = [dir integerValue];
        switch (direction) {
            case DirectionUp:
                _moveUp.enabled = true;
                break;
            case DirectionLeft:
                _moveLeft.enabled = true;
                break;
            case DirectionRight:
                _moveRight.enabled = true;
                break;
            case DirectionDown:
                _moveDown.enabled = true;
                break;
            default:
                break;
        }
    }
    
    PlayerNew* playerInfo = [_gameEngine player];
    [_playerInfoView updateWithPlayer:playerInfo];
    
    RoomNew* room = [_gameEngine currentRoom];
    [_roomItemsView updateWithRoom:room];
}

#pragma mark - RoomItemsViewDelegate methods
- (void) roomItemsView:(RoomItemsView *)roomView didSelectItem:(Item*)item
{
    [_gameEngine didSelectItemInRoom:item];
    [_playerInfoView updateWithPlayer:_gameEngine.player];
    [_roomItemsView updateWithRoom:_gameEngine.currentRoom];
    [_playerInfoView reloadData];
}

#pragma mark - PlayerInfoViewDelegate methods
-(void) playerInfoView:(PlayerInfoView *)playerView didSelectItem:(Item*)item {
    [_playerInfoView reloadData];
    [_roomItemsView reloadData];
}

#pragma mark - GameEngine methods
-(void) endGame:(GameEngine *)gameEngine {
    if (gameEngine.player.gameState == GameStateLose) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Loser!!!"
                                                        message:@"Unfortunately you loose maan(."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [_playerInfoView setUserInteractionEnabled:NO];
        [_roomItemsView setUserInteractionEnabled:NO];
        _moveDown.enabled = false;
        _moveUp.enabled = false;
        _moveRight.enabled = false;
        _moveLeft.enabled = false;
    } else if (gameEngine.player.gameState == GameStateWin) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Winner!!!"
                                                        message:@"My congrats you win!!!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [_playerInfoView setUserInteractionEnabled:NO];
        [_roomItemsView setUserInteractionEnabled:NO];
        _moveDown.enabled = false;
        _moveUp.enabled = false;
        _moveRight.enabled = false;
        _moveLeft.enabled = false;
    }
}

@end
