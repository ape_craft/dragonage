//
//  Item.h
//  DragonAge
//
//  Created by user on 1/17/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject 

@property NSUInteger orderInRoom;

//@property (strong, nonatomic) UIImage* img;
-(id) initWithNameAndOppositeItemName:(NSString*) name OppositeItemName: (NSString*) oppositeItemName;
-(id) initWithDefaultValues;

-(Class) oppositeItemClass;
-(void) setOppositeItemClass: (NSString*) oppositeItemClass;

@end
