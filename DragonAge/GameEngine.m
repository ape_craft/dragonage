//
//  GameEngine.m
//  DragonAge
//
//  Created by user on 1/17/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "GameEngine.h"
#import "MazeNew.h"
#import "Chest.h"
#import "Key.h"
#import "Sword.h"
#import "Stone.h"
#import "Bone.h"
#import "Dragon.h"
#import "Mushroom.h"

@interface GameEngine () {
    NSMutableArray* _itemsTypeArray;
    PlayerNew* _player;
    RoomNew* _currentRoom;
}
@property(nonatomic, retain) PlayerNew* player;
@property (nonatomic, retain) RoomNew* currentRoom;
@end

@implementation GameEngine

@synthesize itemsTypeArray = _itemsTypeArray;
@synthesize player = _player;
@synthesize currentRoom = _currentRoom;

-(id) initEngineAndItems {
    self = [super init];
    
    // When adding new item should via items that are already created
    // this make it easy to check does item is still in our array
    // by means of addresses, also it would be easy to remove items from array
    // also it saves our memory from additionally creating new items
    _itemsTypeArray = [[NSMutableArray alloc] init];
    [_itemsTypeArray addObject:@"Chest"];
    [_itemsTypeArray addObject:@"Key"];
    [_itemsTypeArray addObject:@"Sword"];
    [_itemsTypeArray addObject:@"Stone"];
    [_itemsTypeArray addObject:@"Bone"];
    [_itemsTypeArray addObject:@"Dragon"];
    [_itemsTypeArray addObject:@"Mushroom"];
    
    NSLog(@"Initialized Game Engine");
    
    return self;
}

-(void) startNewGame {

    MazeNew* maze = [[MazeNew alloc]initWithNumberOfRoomsAndItems:13 ItemsArray:_itemsTypeArray];
    _currentRoom = [maze getFirstRoom];
    _player = [[PlayerNew alloc] initWithNumberOfMoves:maze.movesNeededToWin];

    NSLog(@"startGameWithPlayer successfully generated maze and its Rooms");
//    [_itemsTypeArray release];
    [maze dealloc];
}

-(void) goToNextRoomWithDirection:(Direction)direction {
    NSLog(@"Entering next Room with direction: %d", direction);
    if (_player.movesLeft < 1) {
//        _player.gameState = GameStateLose;
        if([self.delegate respondsToSelector:@selector(endGame:)])
        {
            [self.delegate endGame:self];
        }
    }
    _player.movesLeft--;
    _currentRoom = [_currentRoom.directions objectForKey:@(direction)];
}

-(void) didSelectItemInRoom: (Item*) item {
    NSLog(@"Did selected item: %@", NSStringFromClass([item class]));
    
    if ([_player addItem:item]) {
        [_currentRoom removeItem:item];
    }
    if (_player.gameState == GameStateLose || _player.gameState == GameStateWin) {
        if([self.delegate respondsToSelector:@selector(endGame:)])
        {
            [self.delegate endGame:self];
        }
    }
}

-(void) dealloc {
    NSLog(@"Deallocing GameEngine");
    
    [_player release];
    [_currentRoom release];
    [_itemsTypeArray release];
    [super dealloc];
}

@end
