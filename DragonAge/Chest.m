//
//  Chest.m
//  DragonAge
//
//  Created by user on 1/20/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "Chest.h"

@implementation Chest

-(id) initWithDefaultValues
{
    self = [super init];
    [self setOppositeItemClass:@"Key"];
    return self;
}

@end
