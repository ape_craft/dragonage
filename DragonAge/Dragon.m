//
//  Dragon.m
//  DragonAge
//
//  Created by user on 1/20/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "Dragon.h"

@implementation Dragon

-(id) initWithDefaultValues
{
    self = [super init];
    [self setOppositeItemClass:@"Sword"];
    return self;
}
@end
