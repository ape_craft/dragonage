//
//  MazeNew.h
//  DragonAge
//
//  Created by user on 1/17/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RoomNew;

@interface MazeNew : NSObject

//@property (strong, nonatomic) NSArray* roomProperties;
//@property (strong, nonatomic) NSArray* map;
@property ( nonatomic) NSInteger movesNeededToWin;

-(RoomNew*) getFirstRoom;
-(id)initWithNumberOfRoomsAndItems: (int)numberOfRooms ItemsArray: (NSArray*) itemsArray;


@end
