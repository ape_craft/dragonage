//
//  ItemDescription.h
//  DragonAge
//
//  Created by user on 1/17/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ItemDescription : NSObject

@property (strong, nonatomic, readonly) NSString* name;
@property (nonatomic, readonly) int countItem;

-(id) initWithNameCount:(NSString*) name Count:(int) count;

@end
