//
//  AppDelegate.h
//  DragonAge
//
//  Created by user on 12/19/13.
//  Copyright (c) 2013 tz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MainViewController *viewController;

@end
