//
//  RoomItemsView.m
//  DragonAge
//
//  Created by user on 12/23/13.
//  Copyright (c) 2013 tz. All rights reserved.
//

#import "RoomItemsView.h"

@interface RoomItemsView()<UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UITableView* _itemsListTableView;
    RoomNew* _room;
}

@end

@implementation RoomItemsView

@synthesize delegate = _delegate;

+(id) roomItemsView{
    RoomItemsView *roomItemsView = [[[NSBundle mainBundle] loadNibNamed:@"RoomItemsView" owner:nil options:nil] lastObject];
    if ([roomItemsView isKindOfClass:[RoomItemsView class]]) {
        return roomItemsView;
    } else {
        return nil;
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[_room allItems] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* idenitfier = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:idenitfier];
    if(cell==nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenitfier] autorelease];
    }
    id item = [[_room allItems] objectAtIndex:indexPath.row];
//    NSString* itemClassString = NSStringFromClass([item class]);
    cell.textLabel.text = NSLocalizedString(NSStringFromClass([item class]), nil);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([self.delegate respondsToSelector:@selector(roomItemsView:didSelectItem:)])
    {
        [self.delegate roomItemsView:self didSelectItem:[[_room allItems] objectAtIndex:indexPath.row]];
    }
//    [_itemsListTableView reloadData];
}

-(void) reloadData {
    [_itemsListTableView reloadData];
}

- (void) updateWithRoom:(RoomNew*) room
{
    _room = room;
    [self reloadData];
}

-(void) dealloc {
    [_room release];
    [_itemsListTableView release];
    [super dealloc];
}

@end
