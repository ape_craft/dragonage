//
//  Item.m
//  DragonAge
//
//  Created by user on 1/17/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "Item.h"

@interface Item() {
    Class _oppositeItemClass;
    NSString *_itemName;
    NSUInteger _orderInRoom;
}
@property (strong, nonatomic) NSString* itemName;

@end

@implementation Item

@synthesize orderInRoom = _orderInRoom;

-(id) initWithNameAndOppositeItemName:(NSString *)name OppositeItemName:(NSString *)oppositeItemName{
    self = [super init];
    _itemName = name;
    _oppositeItemClass = [[[NSClassFromString(oppositeItemName) alloc] autorelease]class];
    return self;
}

-(id)initWithDefaultValues
{
    self = [super init];
    _orderInRoom = 0;
    return self;
}

- (BOOL) isEqual:(NSString* )object
{
    return [NSStringFromClass([self class]) isEqual:object];
}

- (NSUInteger) hash
{
    return NSStringFromClass([self class]).hash * _orderInRoom;
}

-(Class) oppositeItemClass
{
    return _oppositeItemClass;
}

-(void) setOppositeItemClass:(NSString *)oppositeItemClass
{
    _orderInRoom = 0;
    _oppositeItemClass = [[[NSClassFromString(oppositeItemClass) alloc] autorelease]class];
}

-(void) dealloc {
    [_itemName release];
    [_oppositeItemClass release];
    [super dealloc];
}

@end
