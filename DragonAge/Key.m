//
//  Key.m
//  DragonAge
//
//  Created by user on 1/20/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "Key.h"

@implementation Key

-(id) initWithDefaultValues
{
    self = [super init];
    [self setOppositeItemClass:@"Chest"];
    return self;
}

@end
