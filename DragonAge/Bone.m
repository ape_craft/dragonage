//
//  Bone.m
//  DragonAge
//
//  Created by user on 1/20/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "Bone.h"

@implementation Bone

-(id) initWithDefaultValues
{
    self = [super init];
    [self setOppositeItemClass:nil];
    return self;
}

@end
