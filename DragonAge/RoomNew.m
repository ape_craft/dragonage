//
//  RoomNew.m
//  DragonAge
//
//  Created by user on 1/17/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "RoomNew.h"
#import "Key.h"
#import "Chest.h"
#import "Dragon.h"
#import "Sword.h"
#import "Stone.h"
#import "Bone.h"
#import "Mushroom.h"

@interface RoomNew() {
    NSMutableArray* _roomItemsArray;
    NSMutableDictionary* _roomDirectionsDictionary;
}
@property (retain, nonatomic) NSMutableDictionary* roomDirectionsDictionary;
@property (retain, nonatomic) NSMutableArray* roomItemsArray;

@end

@implementation RoomNew

@synthesize roomDirectionsDictionary = _roomDirectionsDictionary;
@synthesize roomItemsArray = _roomItemsArray;

-(id) init {
    self = [super init];
    _roomDirectionsDictionary = [[NSMutableDictionary alloc]init];
    _roomItemsArray = [[NSMutableArray alloc]init];
    
    NSLog(@"initialized RoomNew");
    return self;
}

-(BOOL) addItem:(Item *)item {
//    [_roomItemsArray containsObject:item];
//    if ([item isKindOfClass:[Key class]]) {
//        NSLog(@"is kind of Key");
//    } else if ([item isKindOfClass:[Mushroom class]]) {
//        NSLog(@"is kind of Mushroom");
//    } else if ([item isKindOfClass:[Dragon class]]) {
//        NSLog(@"is kind of Dragon");
//    } else if ([item isKindOfClass:[Sword class]]) {
//        NSLog(@"is kind of Sword");
//    } else if ([item isKindOfClass:[Stone class]]) {
//        NSLog(@"is kind of Stone");
//    } else if ([item isKindOfClass:[Bone class]]) {
//        NSLog(@"is kind of Bone");
//    } else if ([item isKindOfClass:[Chest class]]) {
//        NSLog(@"is kind of Chest");
//    }
    NSLog(@"Adding item: %@", NSStringFromClass([item class]));
    if ([self containsItemWithClass:[item oppositeItemClass]]) {
        return NO;
    }
    item.orderInRoom = [_roomItemsArray count];
    [_roomItemsArray addObject:item];
    return YES;
}

-(void) removeItem:(Item *)item {
    NSLog(@"Removing item: %@ from room", NSStringFromClass([item class]));
    [_roomItemsArray removeObject:item];
    for (int i = 0; i < [_roomItemsArray count]; i++) {
        Item* itemLeft = _roomItemsArray[i];
        itemLeft.orderInRoom = i;
    }
}

-(NSArray*) allItems {
    NSLog(@"Retrieving all items from Room");
    return _roomItemsArray;
}

-(BOOL) containsItemWithClass:(Class)itemClass {
    NSLog(@"Looking for %@ existance in room", NSStringFromClass(itemClass));
    for (Item* _item in _roomItemsArray) {
        if ([_item isKindOfClass:itemClass]) {
            return YES;
        }
    }
    return NO;
}

-(void) setDirection:(Direction)direction Room:(RoomNew*)room {
    NSLog(@"Setting direction: %d", direction);
    _roomDirectionsDictionary[@(direction)] = room;
}

-(NSDictionary*) directions {
    return _roomDirectionsDictionary;
}

-(void) dealloc {
    [_roomDirectionsDictionary release];
    [_roomItemsArray release];
    [super dealloc];
}

@end
