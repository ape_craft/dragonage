//
//  GameEngine.h
//  DragonAge
//
//  Created by user on 1/17/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayerNew.h"
#import "RoomNew.h"

@class GameEngine;
@protocol GameEngineDelegate <NSObject>

@optional
- (void) endGame:(GameEngine*) gameEngine;

@end

@interface GameEngine : NSObject

@property (readonly) PlayerNew* player;
@property (readonly) RoomNew* currentRoom;
@property (readonly) NSMutableArray* itemsTypeArray;
@property (assign, nonatomic) id<GameEngineDelegate> delegate;

-(id) initEngineAndItems;
-(void) startNewGame;

-(void) goToNextRoomWithDirection:(Direction)direction;
-(void) didSelectItemInRoom: (Item*) item;
@end
