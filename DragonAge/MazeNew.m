//
//  MazeNew.m
//  DragonAge
//
//  Created by user on 1/17/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "MazeNew.h"
#import "RoomNew.h"
#import "PlayerNew.h"
#import "Item.h"
#import "Key.h"
#import "Chest.h"


@interface MazeNew() {
    NSMutableArray *_mazeMap;
    NSMutableArray *_visitedRooms;
    NSMutableArray *_roomCharacteristics;
    NSArray* _itemsTypeArray;
    NSInteger _movesNeededToWin;
}
@property (retain, nonatomic) NSMutableArray* roomCharacteristics;

-(void) generateMaze:(int)numRoom;
-(void) printArray: (NSArray*) array;
-(void) randomize:(int)fromRoom
   EnteringToRoom:(int)toRoom
 DirectionEntered:(Direction)dirEntered;

-(void) inheritProperties:(int)currRoom
                 NextRoom:(int)nextRoom
             StepDeepness:(int)step;

-(void) showMeTheWay:(int) fromRoom EnteringRoom: (int) toRoom;
-(void) assignDirection:(int) from EnteringTo:(int) to Direction: (Direction) dir;
-(void) calculateMoves:(RoomNew*)room
           LookForItem:(Class)itemType
            FoundItems:(NSMutableArray*)items
           VisitedRoom:(NSMutableArray*)visited
          StepDeepness:(int)step;
@end

@implementation MazeNew

//@synthesize map = _mazeMap;
@synthesize roomCharacteristics = _roomCharacteristics;
@synthesize movesNeededToWin = _movesNeededToWin;

-(RoomNew*) getFirstRoom {
    return _roomCharacteristics[0];
}

-(id) initWithNumberOfRoomsAndItems:(int)numberOfRooms ItemsArray:(NSArray *)itemsArray {
    self = [super init];
    _itemsTypeArray = [[NSArray alloc] initWithArray:itemsArray];
    [self generateMaze:numberOfRooms];
    [_itemsTypeArray dealloc];
    
    printf("***************************\n");
    for (int i = 0; i < numberOfRooms; i++) {
        for (int j = 0; j < numberOfRooms; j++) {
            switch ([[[_mazeMap objectAtIndex:i] objectAtIndex:j] integerValue]) {
                case DirectionLeft:
                    [[_roomCharacteristics objectAtIndex:i] setDirection:DirectionLeft Room:[_roomCharacteristics objectAtIndex:j]];
                    break;
                case DirectionUp:
                    [[_roomCharacteristics objectAtIndex:i] setDirection:DirectionUp Room:[_roomCharacteristics objectAtIndex:j]];
                    break;
                case DirectionRight:
                    [[_roomCharacteristics objectAtIndex:i] setDirection:DirectionRight Room:[_roomCharacteristics objectAtIndex:j]];
                    break;
                case DirectionDown:
                    [[_roomCharacteristics objectAtIndex:i] setDirection:DirectionDown Room:[_roomCharacteristics objectAtIndex:j]];
                    break;
                default:
                    break;
            }
        }
        
    }
    NSMutableArray* keys = [[[NSMutableArray alloc]init] autorelease];
    [self calculateMoves:_roomCharacteristics[0] LookForItem:[Key class] FoundItems:keys VisitedRoom:[NSMutableArray array] StepDeepness:0];
    if ([keys count] == 0) {
        int randomRoom = (arc4random() % numberOfRooms + 4)% numberOfRooms;
        RoomNew* room = [_roomCharacteristics objectAtIndex:randomRoom];
        while (![room addItem:[[[Key alloc] initWithDefaultValues] autorelease]]) {
            randomRoom = (arc4random() % numberOfRooms + 4)% numberOfRooms;
            room = [_roomCharacteristics objectAtIndex:randomRoom];
        }
        [self calculateMoves:0 LookForItem:[Key class] FoundItems:keys VisitedRoom:[NSMutableArray array] StepDeepness:0];
    }
    int minKey = numberOfRooms;
    for (int i = 0; i < [keys count]; i++) {
        if (minKey > [[keys objectAtIndex:i]integerValue]) {
            minKey = [[keys objectAtIndex:i]integerValue];
        }
    }
    
    NSMutableArray* chests = [[[NSMutableArray alloc]init]autorelease];
    [self calculateMoves:_roomCharacteristics[0] LookForItem:[Chest class] FoundItems:chests VisitedRoom:[NSMutableArray array] StepDeepness:0];
    if ([chests count] == 0) {
        int randomRoom = (arc4random() % numberOfRooms + 4)% numberOfRooms;
        RoomNew* room = [_roomCharacteristics objectAtIndex:randomRoom];
        
        while (![room addItem:[[[Chest alloc]initWithDefaultValues]autorelease]]) {
            randomRoom = (arc4random() % numberOfRooms + 4)% numberOfRooms;
            room = [_roomCharacteristics objectAtIndex:randomRoom];
        }
        [self calculateMoves:0 LookForItem:[Chest class] FoundItems:chests VisitedRoom:[NSMutableArray array] StepDeepness:0];
    }
    int minChest = numberOfRooms;
    for (int i = 0; i < [chests count]; i++) {
        if (minChest > [[chests objectAtIndex:i]integerValue]) {
            minChest = [[chests objectAtIndex:i]integerValue];
        }
    }
    _movesNeededToWin = minChest + minKey + 5;
    return self;
}

-(void) generateMaze:(int)numRoom{
    _mazeMap = [[NSMutableArray alloc] init];
    _visitedRooms = [[NSMutableArray alloc] init];
    _roomCharacteristics = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < numRoom; i++) {
        NSMutableArray *row = [[[NSMutableArray alloc] initWithCapacity:numRoom]autorelease];
        for (int j = 0; j < numRoom; j++) {
            if (i == j || i == 0 || j == 0)row[j] = @(DirectionBanned);
            else row[j] = @(DirectionDefault);
        }
        [_mazeMap addObject:row];
        [_roomCharacteristics addObject:[[[RoomNew alloc ]init]autorelease]];
        [_visitedRooms addObject:@(NO)];
        
    }
    int to = arc4random() % numRoom;
    to = to == 0 ? 1 : to;
    [_visitedRooms replaceObjectAtIndex:0 withObject:@(YES)];
    
    [self assignDirection:0 EnteringTo:to Direction:DirectionUp];
    [self randomize:0 EnteringToRoom:to DirectionEntered:3];
}

// After user interface completion need to
// correct graph logic in doors rightness
// with relevant coordination respect to
// previous room
// One of the ways to do this is to go back
// for one room and look for some doors which this room can enter
// according to this information, generate a further possibilities
// of the room which we just entered
// and the same step with additional one room back
// but with some minor conditions.

-(void)randomize:(int)fromRoom EnteringToRoom: (int) toRoom DirectionEntered:(Direction) dirEntered {
    
    // Signing room from which we entered as visited
    [_visitedRooms replaceObjectAtIndex:toRoom withObject:@(YES)];
    
    // additional check which is faced bug which should never appear here
    if (dirEntered == DirectionBanned) {
        return;
    }
    
    /// Assignin cross entering doors between two rooms
    /// making them as neighbourhoods
    
    
    /// Generating room properties
    /// Further should add property to monitor where
    /// is located Key and chest and apply some variance to
    /// rise or low game difficulty
    RoomNew *room = [[[RoomNew alloc]init] autorelease];
    int numItems = arc4random() % [_itemsTypeArray count] + 1;
    while (numItems) {
        NSString* className = _itemsTypeArray[arc4random() % [_itemsTypeArray count]];
        id someItem = [[[NSClassFromString(className) alloc] initWithDefaultValues] autorelease];
        while (![room addItem:someItem]) {
            className = _itemsTypeArray[arc4random() % [_itemsTypeArray count]];
            someItem = [[[NSClassFromString(className) alloc] initWithDefaultValues] autorelease];
            continue;
        }
        numItems--;
    }
    
    [_roomCharacteristics replaceObjectAtIndex:toRoom withObject:room];
    
    // Looking for number of room where a user can enter
    int temp = [self numRoomCanEnter];
    if (temp == 0) return;
    
    
    
    // Number of rooms which can enter from one room
    // generated randomly and according to game rules
    // we can only enter from one room only to other three rooms
    int numOfPossibleRooms = (arc4random()%(temp > 3 ? 3 : temp)) + 1;
    for (int i = 0; i < numOfPossibleRooms; i++) {
        
        // Looking for some free unvisited rooms
        // Here we are sure that there must be at least one free room
        // according to function numRoomCanEnter
        int roomIdx = arc4random() % [_visitedRooms count];
        while ([[_visitedRooms objectAtIndex:roomIdx] boolValue] || roomIdx == toRoom) {
            roomIdx = arc4random() %[_visitedRooms count];
        }
        
        // Function maps a direction we suppose to go
        // with the one which we have to go that is tagged before
        [self showMeTheWay:toRoom EnteringRoom:roomIdx];
    }
    
    //    printf("***************************\n");
    //    for (int i = 0; i < [_mazeMap count]; i++) {
    //        [self printArray:_mazeMap[i]];
    //    }
    
    for (int i = 0; i < [_mazeMap count]; i++) {
        if ([_mazeMap[toRoom][i]integerValue] != DirectionDefault && [_mazeMap[toRoom][i]integerValue] != DirectionBanned && i != fromRoom) {
            // This function decides which direction to go next
            // is there is a certain condition where we have to
            // enter a certain room with a certain direction to exclude
            // a mess in our maze
            [self inheritProperties:toRoom NextRoom:i StepDeepness:1];
        }
    }
    
    // Go to the next room recusively and do the same thing
    // with the rest of the rooms.
    for (int i = 0; i < [_mazeMap count]; i++) {
        if (toRoom != i && ([_mazeMap[toRoom][i]integerValue] != DirectionDefault &&
                            [_mazeMap[toRoom][i]integerValue] != DirectionBanned) && ![[_visitedRooms objectAtIndex:i] boolValue]) {
            
            [self randomize:toRoom
             EnteringToRoom:i
           DirectionEntered:[_mazeMap[toRoom][i]integerValue]];
        }
    }
}

-(void) printArray: (NSArray*) array {
    for(int i = 0; i < [array count]; i++) {
        if ([array[i] integerValue] == -1) {
            printf("@ ");
        } else if ([array[i]integerValue] == DirectionDefault){
            printf("* ");
        } else if ([array[i]integerValue] == DirectionLeft) {
            printf("< ");
        } else if ([array[i]integerValue] == DirectionRight) {
            printf("> ");
        } else if ([array[i]integerValue] == DirectionUp) {
            printf("^ ");
        } else if ([array[i]integerValue] == DirectionDown) {
            printf("v ");
        } else {
            printf("%ld ", (long)[array[i] integerValue]);
        }
    }
    printf("\n");
}

-(int) numRoomCanEnter {
    // All bool values are indicated as visited which we can use
    // to easily calculate room those are still unvisited
    int count = 0;
    for (int i = 0; i < [_visitedRooms count]; i++) {
        count += ![[_visitedRooms objectAtIndex:i]boolValue];
    }
    return count;
}

-(void) inheritProperties:(int)currRoom
                 NextRoom:(int)nextRoom
             StepDeepness:(int)step {
    
    if (step >= 3)return;
    
    for (int i = 0; i < [[_mazeMap objectAtIndex:nextRoom]count]; i++) {
        if (step == 1) {
            if (_mazeMap[nextRoom][i] == DirectionDefault && _mazeMap[currRoom][i] != DirectionDefault && i != nextRoom) {
                _mazeMap[nextRoom][i] = @(DirectionBanned);
                _mazeMap[i][nextRoom] = @(DirectionBanned);
            }
        } else {
            switch ([_mazeMap[currRoom][nextRoom] integerValue]) {
                case DirectionUp:
                    if ([_mazeMap[currRoom][i]  isEqual: @(DirectionRight)]) {
                        for (int j = 0; j < [_mazeMap count]; j++) {
                            if ([_mazeMap[i][j] isEqual:@(DirectionUp)]) {
                                [self assignDirection:nextRoom EnteringTo:j Direction:DirectionRightP];
                            }
                        }
                    } else if ([_mazeMap[currRoom][i] isEqual:@(DirectionLeft)]) {
                        for (int j = 0; j < [_mazeMap count]; j++) {
                            if ([_mazeMap[i][j] isEqual:@(DirectionUp)]) {
                                [self assignDirection:nextRoom EnteringTo:j Direction:DirectionLeftP];
                            }
                        }
                    }
                    break;
                case DirectionDown:
                    if ([_mazeMap[currRoom][i]  isEqual: @(DirectionRight)]) {
                        for (int j = 0; j < [_mazeMap count]; j++) {
                            if ([_mazeMap[i][j] isEqual:@(DirectionDown)]) {
                                [self assignDirection:nextRoom EnteringTo:j Direction:DirectionRightP];
                            }
                        }
                    } else if ([_mazeMap[currRoom][i] isEqual:@(DirectionLeft)]) {
                        for (int j = 0; j < [_mazeMap count]; j++) {
                            if ([_mazeMap[i][j] isEqual:@(DirectionDown)]) {
                                [self assignDirection:nextRoom EnteringTo:j Direction:DirectionLeftP];
                            }
                        }
                    }
                    break;
                case DirectionRight:
                    if ([_mazeMap[currRoom][i]  isEqual: @(DirectionUp)]) {
                        for (int j = 0; j < [_mazeMap count]; j++) {
                            if ([_mazeMap[i][j] isEqual:@(DirectionRight)]) {
                                [self assignDirection:nextRoom EnteringTo:j Direction:DirectionUpP];
                            }
                        }
                    } else if ([_mazeMap[currRoom][i] isEqual:@(DirectionDown)]) {
                        for (int j = 0; j < [_mazeMap count]; j++) {
                            if ([_mazeMap[i][j] isEqual:@(DirectionRight)]) {
                                [self assignDirection:nextRoom EnteringTo:j Direction:DirectionDownP];
                            }
                        }
                    }
                    break;
                case DirectionLeft:
                    if ([_mazeMap[currRoom][i]  isEqual: @(DirectionUp)]) {
                        for (int j = 0; j < [_mazeMap count]; j++) {
                            if ([_mazeMap[i][j] isEqual:@(DirectionLeft)]) {
                                [self assignDirection:nextRoom EnteringTo:j Direction:DirectionUpP];
                            }
                        }
                    } else if ([_mazeMap[currRoom][i] isEqual:@(DirectionDown)]) {
                        for (int j = 0; j < [_mazeMap count]; j++) {
                            if ([_mazeMap[i][j] isEqual:@(DirectionLeft)]) {
                                [self assignDirection:nextRoom EnteringTo:j Direction:DirectionDownP];
                            }
                        }
                    }
                    break;
                    
                default:
                    break;
            }
        }
    }
    [self inheritProperties:currRoom NextRoom:nextRoom StepDeepness:++step];
}

-(void) showMeTheWay:(int)fromRoom EnteringRoom:(int)toRoom {
    bool dirs1[4] = {false}, dirs2[4] = {false};
    for (int i = 0; i<[_mazeMap count]; i++) {
        switch ([_mazeMap[fromRoom][i] integerValue]) {
            case DirectionRight:
                dirs1[0] = 1;
                break;
            case DirectionLeft:
                dirs1[1] = 1;
                break;
            case DirectionUp:
                dirs1[2] = 1;
                break;
            case DirectionDown:
                dirs1[3] = 1;
                break;
            case DirectionLeftP:
                if (![_mazeMap[fromRoom][toRoom] isEqual:@(DirectionLeftP)]) {
                    dirs1[1] = 1;
                }
                break;
            case DirectionUpP:
                if (![_mazeMap[fromRoom][toRoom]isEqual:@(DirectionUpP)]) {
                    dirs1[2]=1;
                }
                break;
            case DirectionRightP:
                if (![_mazeMap[fromRoom][toRoom] isEqual:@(DirectionRightP)]) {
                    dirs1[0]=1;
                }
                break;
            case DirectionDownP:
                if (![_mazeMap[fromRoom][toRoom] isEqual:@(DirectionDownP)]) {
                    dirs1[3] = 1;
                }
                break;
        }
        switch ([_mazeMap[toRoom][i] integerValue]) {
            case DirectionRight:
                dirs2[0] = 1;
                break;
            case DirectionLeft:
                dirs2[1] = 1;
                break;
            case DirectionUp:
                dirs2[2] = 1;
                break;
            case DirectionDown:
                dirs2[3] = 1;
                break;
            case DirectionLeftP:
                if (![_mazeMap[toRoom][fromRoom] isEqual:@(DirectionLeftP)]) {
                    dirs2[1] = 1;
                }
                break;
            case DirectionUpP:
                if (![_mazeMap[toRoom][fromRoom]isEqual:@(DirectionUpP)]) {
                    dirs2[2]=1;
                }
                break;
            case DirectionRightP:
                if (![_mazeMap[toRoom][fromRoom] isEqual:@(DirectionRightP)]) {
                    dirs2[0]=1;
                }
                break;
            case DirectionDownP:
                if (![_mazeMap[toRoom][fromRoom] isEqual:@(DirectionDownP)]) {
                    dirs2[3] = 1;
                }
                break;
        }
    }
    if ((dirs1[0] && dirs1[1] && dirs1[2] && dirs1[3]) || (dirs2[0] && dirs2[1] && dirs2[2] && dirs2[3])) {
        return;
    }
    NSMutableSet *dirs = [[[NSMutableSet alloc] init]autorelease];
    int direction = (arc4random() % 2 + 2) * (pow(-1, (arc4random() % 2)));
    while ([_mazeMap[fromRoom] containsObject:@(direction)] ||
           [_mazeMap[toRoom] containsObject:@(direction * -1)]) {
        [dirs addObject:@(direction)];
        if ([dirs count] == 4) {
            return;
        }
        while ([dirs containsObject:@(direction)]) {
            direction = (arc4random() % 2 + 2) * (pow(-1, (arc4random() % 2)));
        }
    }
    switch (direction) {
        case DirectionRight:
            if ([_mazeMap[fromRoom] containsObject:@(DirectionRightP)]) {
                for (int i = 0; i < [_mazeMap[fromRoom] count]; i++) {
                    if ([_mazeMap[fromRoom][i] isEqual: @(DirectionUpP)]) {
                        [self assignDirection:fromRoom EnteringTo:i Direction:DirectionRight];
                    }
                }
            } else {
                [self assignDirection:fromRoom EnteringTo:toRoom Direction:DirectionRight];
            }
            break;
        case DirectionLeft:
            if ([_mazeMap[fromRoom] containsObject:@(DirectionLeftP)]) {
                for (int i = 0; i < [_mazeMap[fromRoom] count]; i++) {
                    if ([_mazeMap[fromRoom][i] isEqual: @(DirectionLeftP)]) {
                        [self assignDirection:fromRoom EnteringTo:i Direction:DirectionLeft];
                    }
                }
            } else {
                [self assignDirection:fromRoom EnteringTo:toRoom Direction:DirectionLeft];
            }
            break;
        case DirectionUp:
            if ([_mazeMap[fromRoom] containsObject:@(DirectionUpP)]) {
                for (int i = 0; i < [_mazeMap[fromRoom] count]; i++) {
                    if ([_mazeMap[fromRoom][i] isEqual: @(DirectionUpP)]) {
                        [self assignDirection:fromRoom EnteringTo:i Direction:DirectionUp];
                    }
                }
            } else {
                [self assignDirection:fromRoom EnteringTo:toRoom Direction:DirectionUp];
            }
            break;
        case DirectionDown:
            if ([_mazeMap[fromRoom] containsObject:@(DirectionDownP)]) {
                for (int i = 0; i < [_mazeMap[fromRoom] count]; i++) {
                    if ([_mazeMap[fromRoom][i] isEqual: @(DirectionDownP)]) {
                        [self assignDirection:fromRoom EnteringTo:i Direction:DirectionDown];
                    }
                }
            } else {
                [self assignDirection:fromRoom EnteringTo:toRoom Direction:DirectionDown];
            }
            break;
    }
}

-(void) assignDirection:(int)from EnteringTo:(int)to Direction:(Direction)dir {
    if (to == 0 || from == 0) {
        printf("Assigning to 0 \n");
    }
    [_mazeMap[from] replaceObjectAtIndex:to withObject:@(dir)];
    [_mazeMap[to] replaceObjectAtIndex:from withObject:@(dir * -1)];
}

-(void) calculateMoves:(RoomNew*)room
           LookForItem:(Class)itemType
            FoundItems:(NSMutableArray*)items
           VisitedRoom:(NSMutableArray*)visited
          StepDeepness:(int)step {
//    int cycle = 0;
//    for (int i = 0; i < [visited count]; i++) {
//        cycle += [[visited objectAtIndex:i] boolValue];
//    }
    if (!room) {
        return;
    }
    if ([visited containsObject:room]) return;
        [visited addObject:room];
    if ([room containsItemWithClass:itemType]) {
        [items addObject:@(step)];
    }
    step++;
    NSDictionary* nextRooms = [room directions];
    for (id nextRoom in nextRooms) {
        RoomNew* nextEnteringRoom = nextRooms[nextRoom];
        if (nextEnteringRoom == nil) {
            NSLog(@"our room is nil pointer.");
        }

        if (![visited containsObject:nextEnteringRoom]) {
            [self calculateMoves:nextEnteringRoom
                     LookForItem:itemType
                      FoundItems:items
                     VisitedRoom:visited
                    StepDeepness:step];
        }
    }
    step--;
}

-(void) dealloc {
    [_mazeMap release];
    [_visitedRooms release];
    [_roomCharacteristics release];
//    [_itemsTypeArray release];
//    [_movesNeededToWin release];
    [super dealloc];
}

@end
