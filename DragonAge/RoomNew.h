//
//  RoomNew.h
//  DragonAge
//
//  Created by user on 1/17/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"

typedef enum Direction: int Direction;

typedef NS_ENUM(NSInteger, Direction)
{
    DirectionBanned = -1,
    DirectionDefault = 0,
    
    DirectionLeft = -2,
    DirectionUp    = 3,
    DirectionRight = 2,
    DirectionDown = -3,
    
    DirectionLeftP = -4,
    DirectionUpP    = 5,
    DirectionRightP = 4,
    DirectionDownP = -5,
    
    DirectionLeftM = -6,
    DirectionUpM = 7,
    DirectionRightM = 6,
    DirectionDownM = -7
};

@interface RoomNew : NSObject

@property (readonly) NSMutableDictionary* roomDirectionsDictionary;
@property (readonly) NSMutableArray* roomItemsArray;

-(id) init;

-(BOOL) addItem:(Item*)item;
-(void) removeItem:(Item*) item;
-(BOOL) containsItemWithClass:(Class) itemClass;
-(NSArray*) allItems;
-(void) setDirection:(Direction) direction Room:(RoomNew*) room;
-(NSDictionary*) directions;

@end
