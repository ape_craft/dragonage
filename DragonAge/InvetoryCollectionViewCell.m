//
//  CustomCollectionView.m
//  DragonAge
//
//  Created by user on 1/8/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "InvetoryCollectionViewCell.h"

@implementation InvetoryCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc {
    [_img release];
    [_countItem release];
    [super dealloc];
}
@end
