//
//  PlayerNew.h
//  DragonAge
//
//  Created by user on 1/17/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"

typedef enum GameState:int GameState;

typedef NS_ENUM(NSInteger, GameState)
{
    GameStateLose,
    GameStateAlive,
    GameStateWin
    
};

@interface PlayerNew : NSObject

@property int movesLeft;

-(id) initWithNumberOfMoves:(NSInteger) numberOfMoves;

-(BOOL) addItem:(Item* ) item;
-(void) removeItem:(Item*) item;
-(NSArray*)allItems;
-(int) getMovesLeft;
-(void)setMoves:(int) moves;
-(GameState) gameState;

@end
