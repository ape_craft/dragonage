//
//  ItemDescription.m
//  DragonAge
//
//  Created by user on 1/17/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "ItemDescription.h"

@implementation ItemDescription

-(id)initWithNameCount:(NSString *)name Count:(int)count {
    self = [super init];
    _name = name;
    _countItem = count;
    return self;
}

-(BOOL) isEqual:(ItemDescription*)object
{
    return [_name isEqual:object.name];
}

-(NSUInteger) hash
{
    return _name.hash;
}

-(void) dealloc {
    [_name release];
//    [_count release];
    [super dealloc];
}

@end
