//
//  RoomItemsView.h
//  DragonAge
//
//  Created by user on 12/23/13.
//  Copyright (c) 2013 tz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomNew.h"

@class Item;
@class RoomItemsView;

@protocol RoomItemsViewDelegate <NSObject>

@optional
- (void) roomItemsView:(RoomItemsView*) roomView didSelectItem:(Item*) item;

@end

@interface RoomItemsView : UIView

@property(nonatomic, assign) id<RoomItemsViewDelegate> delegate;

+(id) roomItemsView;
- (void) updateWithRoom:(RoomNew*) room;
- (void) reloadData;

@end
