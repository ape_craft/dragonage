//
//  PlayerNew.m
//  DragonAge
//
//  Created by user on 1/17/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "PlayerNew.h"
#import "ItemDescription.h"
#import "Dragon.h"
#import "Chest.h"
#import "Sword.h"
#import "Key.h"

@interface PlayerNew() {
    NSCountedSet* _inventoryItemsCountedSet;
    int _movesLeft;
    GameState _gameState;
}

@end

@implementation PlayerNew

-(id) init
{
    self = [super init];
    _inventoryItemsCountedSet = [[NSCountedSet alloc] init];
    _gameState = GameStateAlive;
    return self;
}

-(id) initWithNumberOfMoves:(NSInteger)numberOfMoves
{
    self = [super init];
    _inventoryItemsCountedSet = [[NSCountedSet alloc] init];
    _gameState = GameStateAlive;
    _movesLeft = numberOfMoves;
    return self;
}

-(BOOL) addItem:(Item *)item {
    NSLog(@"Adding new item to inventory: %@", NSStringFromClass([item class]));
    
    if ([item isKindOfClass:[Dragon class]]) {
        if ([_inventoryItemsCountedSet containsObject:NSStringFromClass([item oppositeItemClass])]) {
            _movesLeft++;
            [_inventoryItemsCountedSet removeObject:NSStringFromClass([item oppositeItemClass])];
            return YES;
        } else {
            _gameState = GameStateLose;
            return NO;
        }
    }
    
    if ([item isKindOfClass:[Chest class]]) {
        if ([_inventoryItemsCountedSet containsObject:NSStringFromClass([item oppositeItemClass])]) {
            _gameState = GameStateWin;
            return YES;
        } else {
            return NO;
        }
    }
    item.orderInRoom = 0;
    [_inventoryItemsCountedSet addObject:NSStringFromClass([item class])];
    return YES;
}

-(void) removeItem:(Item *)item {
    [_inventoryItemsCountedSet removeObject:item];
}

-(NSArray*) allItems {
    NSMutableArray* itemsCount = [[[NSMutableArray alloc] init] autorelease];
    for (id item in _inventoryItemsCountedSet)
    {
        ItemDescription* itemDescription = [[[ItemDescription alloc] initWithNameCount:item Count:[_inventoryItemsCountedSet countForObject:item]] autorelease];
        [itemsCount addObject:itemDescription];
//        NSLog(@"Name=%@, Count=%lu", item, (unsigned long)[_inventoryItemsCountedSet countForObject:item]);
    }
    return [itemsCount mutableCopy];
}

-(int) getMovesLeft {
    return _movesLeft;
}

-(void) setMoves:(int)moves {
    _movesLeft = moves;
}

-(GameState) gameState {
    if (_movesLeft > 0) {
        return  _gameState;
    } else {
        return GameStateLose;
    }
}

-(void) dealloc {
    [_inventoryItemsCountedSet release];
//    [_movesLeft release];
//    [_gameState release];
    [super dealloc];
}

@end
