//
//  CustomCollectionView.h
//  DragonAge
//
//  Created by user on 1/8/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvetoryCollectionViewCell : UICollectionViewCell
@property (retain, nonatomic) IBOutlet UILabel *countItem;
@property (retain, nonatomic) IBOutlet UIImageView *img;

@end
