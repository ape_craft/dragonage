//
//  Sword.m
//  DragonAge
//
//  Created by user on 1/20/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "Sword.h"

@implementation Sword

-(id) initWithDefaultValues
{
    self = [super init];
    [self setOppositeItemClass:@"Dragon"];
    return self;
}

@end
